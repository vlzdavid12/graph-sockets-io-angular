import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';



@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  public socketStatus = false;
  public user = null;

  constructor(
    private socket: Socket
  ) {
    this.checkStatus();
  }


    checkStatus(): void {

      this.socket.on('connect', () => {
        console.log('Conectado al servidor');
        this.socketStatus = true;
      });

      this.socket.on('disconnect', () => {
        console.log('Desconectado del servidor');
        this.socketStatus = false;
      });
    }


    emit( event: string, payload?: any, callback?: Function ): void {

      console.log('Emit...', event);
      // emit('EVENT', payload, callback?)
      this.socket.emit( event, payload, callback );

    }

    listen( event: string ): any {
      return this.socket.fromEvent( event );
    }



}
