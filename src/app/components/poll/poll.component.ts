import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataset } from 'chart.js';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {WebsocketService} from '../../services/websocket.service';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {


  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: string[] = ['Question No.1', 'Question No.2', 'Question No.3', 'Question No.4'];
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [];

  public barChartData: any[] = [
    { data: [8, 9, 0, 5], label: 'Interview' },
  ];


  constructor(private  http: HttpClient,
              public wsService: WebsocketService) {
    this.callSockets();
  }

  ngOnInit(): void {
    const url: string = environment.urlAPI;
    this.http.get(`${url}/graph`).subscribe( (resp: any) => this.barChartData = resp);
  }

  callSockets(): void{
    this.wsService.listen('change-graph').subscribe((resp: any) => {
      this.barChartData = resp});
  }



}
